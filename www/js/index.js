//var URL_REST = "http://app01.jmsoft.cloudfacil.net/janalisa/";
// var URL_REST = "http://jmserver01/janalisa/security/event/";
// var URL_REST_LOGIN = "http://jmserver01/janalisa/security/";
var URL_REST = "http://localhost:1214/janalisa/security/event/";
var URL_REST_LOGIN = "http://localhost:1214/janalisa/security/";

var PAGESIZE = 0;
var COUNT = 7;


var jmSoftApp = angular.module('jAnalisa', ['onsen', 'ngCordova']);
jmSoftApp.controller('AppController', function($scope) {
  $scope.title = "jAnalisa";
  $scope.fn = {};

  ons.bootstrap();

  document.addEventListener('deviceready', function() {
  // Enable to debug issues.
 //window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

   var notificationOpenedCallback = function(jsonData) {
    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal
    .startInit("c7ccf0b6-f673-433e-80f3-5bffb84a245d")
    .handleNotificationOpened(notificationOpenedCallback)
    .endInit();

    window.plugins.OneSignal.getIds(function(ids) {
     console.log('getIds: ' + JSON.stringify(ids));
     //alert("userId = " + ids.userId + ", pushToken = " + ids.pushToken);
     $scope.MacIdUser = ids.userId;
   });



  // Call syncHashedEmail anywhere in your app if you have the user's email.
  // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
  // window.plugins.OneSignal.syncHashedEmail(userEmail);
  }, false);

  $scope.fn.open = function() {
    var menu = document.getElementById('menu');
    menu.toggle();
  };

  $scope.exit = function(){
    ons.notification.confirm({message: 'Tem certeza que deseja sair?'}).then(function(buttonIndex){
      if(buttonIndex === 1) {
        localStorage.clear();
        $navigator.resetToPage('views/login.html');
      }
    });
  }

  $scope.fn.load = function(page) {
    var content = document.getElementById('content');
    var menu = document.getElementById('menu');
    content.load(page)
      .then(menu.close.bind(menu));
  };

});

jmSoftApp.value("jmSoftAppVal", {
  token: null,
  user: {
    id: null,
    name: null
  }
});

jmSoftApp.run(function(jmSoftAppVal, $http){ // quando app fazer boot
  setTimeout(function(){
      if(window.localStorage.jmSoftAppVal) {
        var appVal = JSON.parse(window.localStorage.jmSoftAppVal);

        jmSoftAppVal.token = appVal.token;
        jmSoftAppVal.user.id = appVal.user.id;
        jmSoftAppVal.user.name = appVal.user.name;

        $http.defaults.headers.common.Authorization = 'Token ' + jmSoftAppVal.token;
        $http.defaults.headers.common.ContentType = 'application/json';

        $navigator.pushPage('views/menu.html');
      } else {
        $navigator.pushPage('views/login.html');
      }
  }, 100);
});
