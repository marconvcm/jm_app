jmSoftApp.controller('InscricaoEventoController', function($scope, EventService, EventParticipantService, jmSoftAppVal) {

    $scope.loadEnrolledEvents = function(){
      EventService.getEnrolledEvents(jmSoftAppVal.user.id).then(function(response){
        $scope.eventsEnrolled = response.data;
      }).catch(function(responseError){
        // alert error
      });
    }

    $scope.loadEnrolledEvents();
    $scope.loadEnrolledEvents();
    $scope.navigateToLogin = function(){
      $navigator.pushPage('views/menu.html');
    }

    $scope.loadEnrolled = function(check){
      if(check == 1){
        return true;
      } else {
        return false;
      }
    }

    $scope.enrolled = function(event, eventList){
      if(eventList != null){
        var eventEnrolled = JSON.stringify({"IdEvent":eventList.IdEvent});
        var id = jmSoftAppVal.user.id;
        if(event){
          EventParticipantService.enrolledParticipant(id, eventEnrolled).then(function(response){
            alert('Parabéns você foi  inscrito no evento!');
          }).catch(function(responseError){
              // alert error
          });
        }else{
          EventParticipantService.unEnrolledParticipant(id, eventList.IdEvent).then(function(response){
            alert('Você saiu deste evento!');
          }).catch(function(responseError){
              // alert error
          });
        }
        $scope.loadEnrolledEvents();
      }
    };
});
