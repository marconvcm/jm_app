jmSoftApp.controller('LoginController', function($scope, LoginService, jmSoftAppVal, $http) {
$scope.invalid = false;

  $scope.navigateToLogin = function(){
      var mail = $scope.username;
      var pass = $scope.password;
      var macAdd = $scope.MacIdUser;

      $scope.user = JSON.stringify({"mail":mail, "password":pass, "macid": macAdd});
      LoginService.loginData($scope.user).then(function(response){
        $navigator.pushPage('views/menu.html');
      }).catch(function(responseError){
        // alert error
        alert(responseError.data);
        alert(responseError.statusText);
        $scope.invalid = true;
      });
  }
});
