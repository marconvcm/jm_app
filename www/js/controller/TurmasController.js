jmSoftApp.controller('TurmasController', function($scope, EventDayService) {
  $scope.invalid_turmas = false;

  $scope.navigateToTurma = function(details){
    $navigator.pushPage('views/detalhes.html', { data: { id: details.Id } });
  }

  $scope.today = function(){
    EventDayService.getToday().then(function(response){
      $scope.events = response.data;
      if (response.data.length == 0)
        $scope.invalid_turmas = true;
      else
        $scope.invalid_turmas = false;

    }).catch(function(responseError){
      // alert error
    });
  }

  $scope.previous = function(){

    EventDayService.getPrevious(COUNT,PAGESIZE).then(function(response){
      $scope.events = response.data;
      if (response.data.length == 0)
        $scope.invalid_turmas = true;
      else
        $scope.invalid_turmas = false;
    }).catch(function(responseError){
      // alert error
    });
  }

  $scope.next = function(){

    EventDayService.getNext(COUNT,PAGESIZE).then(function(response){
      $scope.events = response.data;
      if (response.data.length == 0)
        $scope.invalid_turmas = true;
      else
        $scope.invalid_turmas = false;
    }).catch(function(responseError){
      // alert error
    });
  }
  $scope.today();

});
