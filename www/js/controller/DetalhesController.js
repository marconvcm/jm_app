jmSoftApp.controller('DetalhesController', function($scope, EventService, jmSoftAppVal) {
  var id = $navigator.topPage.data.id;
  $scope.navigateToAluno = function(turma){
    $navigator.pushPage('views/alunos.html', { data: { id: turma.Id } });
  }

  EventService.getEventDetail(id).then(function(response){

    $scope.eventsDetails = response.data;
    $scope.geolocation = response.data.Geolocation;
    $scope.map;
    $scope.markers = [];
    $scope.markerId = 1;

    function mapInit(){
         var local = $scope.geolocation.split(",");
          var latlng = new google.maps.LatLng(local[0],local[1]);
          var myOptions = {
              zoom: 8,
              center: latlng,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          $scope.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
          $scope.overlay = new google.maps.OverlayView();
          $scope.overlay.draw = function() {}; // empty function required
          $scope.overlay.setMap($scope.map);
          $scope.element = document.getElementById('map_canvas');
      }

     mapInit();

  }).catch(function(responseError){
    // alert error
  });

});
