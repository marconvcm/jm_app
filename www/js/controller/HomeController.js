jmSoftApp.controller('HomeController', function($scope, jmSoftAppVal, EventParticipantService) {
  $scope.NamePerson = jmSoftAppVal.user.name;

  $scope.aval = function(item){
    $scope.rating = item;
    var rat1 = document.getElementById('rat1');
    var rat2 = document.getElementById('rat2');
    var rat3 = document.getElementById('rat3');
    var rat4 = document.getElementById('rat4');
    var rat5 = document.getElementById('rat5');
    if(item == 1){
        rat1.style.color = "#BBBB00";
        rat2.style.color = "#FFFFFF";
        rat3.style.color = "#FFFFFF";
        rat4.style.color = "#FFFFFF";
        rat5.style.color = "#FFFFFF";
      }else if(item == 2){
        rat1.style.color = "#BBBB00";
        rat2.style.color = "#BBBB00";
        rat3.style.color = "#FFFFFF";
        rat4.style.color = "#FFFFFF";
        rat5.style.color = "#FFFFFF";
      }else if(item == 3){
        rat1.style.color = "#BBBB00";
        rat2.style.color = "#BBBB00";
        rat3.style.color = "#BBBB00";
        rat4.style.color = "#FFFFFF";
        rat5.style.color = "#FFFFFF";
      }else if(item == 4){
        rat1.style.color = "#BBBB00";
        rat2.style.color = "#BBBB00";
        rat3.style.color = "#BBBB00";
        rat4.style.color = "#BBBB00";
        rat5.style.color = "#FFFFFF";
      }else if(item == 5){
        rat1.style.color = "#BBBB00";
        rat2.style.color = "#BBBB00";
        rat3.style.color = "#BBBB00";
        rat4.style.color = "#BBBB00";
        rat5.style.color = "#BBBB00";
      }
  };
  $scope.listAvailable = function(){
    EventParticipantService.getAvailables(jmSoftAppVal.user.id).then(function(response){
      if(response.data != "null"){
        var modalAvaReacao = document.getElementById('modalAvaReacao');
        modalAvaReacao.show();
        $scope.idEventParticipant = response.data.IdEventParticipant;
        $scope.NameEventAvailable = response.data.NameEvent;
      }
    }).catch(function(responseError){
       // alert error
    });
  }

  $scope.listAvailable();

  $scope.available = function(){
    if($scope.rating == null){
        alert('Você deve informar sua avalição de uma a cinco estrelas');
        return;
      }
    var rating = $scope.rating;
    var comment= $scope.comment;
    var eventParticipant = JSON.stringify({"Rating": rating, "Comment": comment });
    EventParticipantService.sendAvailable($scope.idEventParticipant, eventParticipant).then(function(response){
      location.reload();
    }).catch(function(responseError){
       // alert error
    });
  }

    $scope.EventsRealizedGet = function(){
      EventParticipantService.getEventsRealized(jmSoftAppVal.user.id).then(function(response){
        $scope.EventsRealized = response.data;
      }).catch(function(responseError){
         // alert error
    });
  }

  $scope.RatingGet = function(){
      EventParticipantService.getRating(jmSoftAppVal.user.id).then(function(response){
        $scope.Rating = response.data;
        $scope.LoadChartGauge();
      }).catch(function(responseError){
         // alert error
    });
  }
    $scope.HoursMonthGet = function(){
      EventParticipantService.getHoursMonth(jmSoftAppVal.user.id).then(function(response){
        $scope.HoursMonth = response.data;
        console.log($scope.HoursMonth);
      }).catch(function(responseError){
         // alert error
    });
  }
    $scope.EventsTrainerGet = function(){
      EventParticipantService.getEventsTrainer(jmSoftAppVal.user.id).then(function(response){
        $scope.EventsTrainer = response.data;
      }).catch(function(responseError){
         // alert error
    });
  }
    $scope.RatingTrainerGet = function(){
      EventParticipantService.getRatingTrainer(jmSoftAppVal.user.id).then(function(response){
        $scope.RatingTrainer = response.data;
      }).catch(function(responseError){
         // alert error
    });
  }
    $scope.HoursMonthTrainerGet = function(){
      EventParticipantService.getHoursMonthTrainer(jmSoftAppVal.user.id).then(function(response){
        $scope.HoursMonthTrainer = response.data;
      }).catch(function(responseError){
         // alert error
    });
  }

  //var data = $scope.HoursMonth; // We'll leave this empty for now.
  $scope.LoadChartGauge = function(){
    Highcharts.chart('container', {

      chart: {
          type: 'gauge',
          plotBackgroundColor: null,
          plotBackgroundImage: null,
          plotBorderWidth: 0,
          plotShadow: false
      },

      title: {
          text: 'Satisfação Eventos Realizado'
      },

      pane: {
          startAngle: -150,
          endAngle: 150,
          background: [{
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                      [0, '#6495ED'],
                      [1, '#6495ED']
                  ]
              },
              borderWidth: 0,
              outerRadius: '109%'
          }, {
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                      [0, '#6495ED'],
                      [1, '#6495ED']
                  ]
              },
              borderWidth: 1,
              outerRadius: '107%'
          }, {
              // default background
          }, {
              backgroundColor: '#6495ED',
              borderWidth: 0,
              outerRadius: '105%',
              innerRadius: '103%'
          }]
      },

      // the value axis
      yAxis: {
          min: 0,
          max: 5,

          minorTickInterval: 'auto',
          minorTickWidth: 1,
          minorTickLength: 1,
          minorTickPosition: 'inside',
          minorTickColor: '#6495ED',

          tickPixelInterval: 0.5,
          tickWidth: 5,
          tickPosition: 'inside',
          tickLength: 5,
          tickColor: '#6495ED',
          labels: {
              step: 5,
              rotation: 'auto'
          },
          title: {
              text: 'Nota Média'
          },
          plotBands: [{
              from: 0,
              to: 2.5,
              color: '#000080' // green
          }, {
              from: 2.5,
              to: 4,
              color: '#000080' // yellow
          }, {
              from: 4,
              to: 5,
              color: '#000080' // red
          }]
      },

      series: [{
          name: 'Speed',
          data: [parseFloat($scope.Rating)],
          tooltip: {
              valueSuffix: ' Nota'
          }
      }]

  },
  // Add some life
  function (chart) {
      if (!chart.renderer.forExport) {
        var point = chart.series[0].points[0],
            newVal,
            inc = $scope.Rating;

        newVal = inc;

        point.update(newVal);
      }

    });
  }

  $scope.LoadChartLines = function(){
    var data = []
    for(var i = 1; i <= Math.E; i += 0.01) {
      data.push({x: i, y: Math.log(i)});
    }

    var data = [{
      key: 'y = log(x)',
      values: data
    }];

    nv.addGraph(function() {
      var chart = nv.models.lineChart()
        .showLegend(false)
        .showYAxis(true)
        .showXAxis(true);

      chart.xAxis
        .axisLabel('x')
        .tickFormat(d3.format('.2f'));

      chart.yAxis
        .axisLabel('y')
        .tickFormat(d3.format('.2f'));

      d3.select('svg')
        .datum(data)
        .call(chart);

      nv.utils.windowResize(function() {
        chart.update()
      });

      return chart;
    });

  }

  // Call Services Indicator
  $scope.EventsRealizedGet();
  $scope.RatingGet();
  $scope.HoursMonthGet();
  $scope.EventsTrainerGet();
  $scope.RatingTrainerGet();
  $scope.HoursMonthTrainerGet();
  $scope.LoadChartLines();
});
