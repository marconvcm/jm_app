jmSoftApp.controller('AlunosController', function($scope, EventParticipantService) {
 var id = $navigator.topPage.data.id;

  EventParticipantService.getParticipants(id).then(function(response){
    $scope.participants = response.data;
  }).catch(function(responseError){
    // alert error
  });

  $scope.loadPresence = function(check){
    if(check == 1){
      return true;
    } else {
      return false;
    }
  }
  $scope.presence = function(event, aluno){
    var params;
    if(event){
      params   = { "Present" : 1 };
    } else {
      params   = { "Present" : 0 };
    }

    EventParticipantService.putPresence(aluno.Id, params).then(function(response){

    }).catch(function(responseError){
       // alert error
    });
  }

});
