jmSoftApp.service("EventDayService", function($http, jmSoftAppVal){

  return {
    getToday: function() {
      return $http.get(URL_REST + 'eventday/' + jmSoftAppVal.user.id + '/today');
    },

    getPrevious: function(count, page) {
      return $http.get(URL_REST + 'eventday/' + jmSoftAppVal.user.id + '/previous/' + count + '/' + page);
    },

    getNext: function(count, page) {
      return $http.get(URL_REST + 'eventday/' + jmSoftAppVal.user.id + '/next/' + count + '/' + page);
    },
  }


});
