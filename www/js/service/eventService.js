jmSoftApp.service("EventService", function($http, jmSoftAppVal){

  return {
    getEventDetail: function(id) {
      return $http.get(URL_REST + 'event/details/' + id);
    },

    getEnrolledEvents: function(id) {
      return $http.get(URL_REST + 'event/enrolleds/' + id);
    }
  }
});
