jmSoftApp.service("EventParticipantService", function($http){

  return {
    getParticipants: function(id) {
      return $http.get(URL_REST + 'eventparticipantpresence/' + id);
    },

    putPresence: function(id, params) {
      return $http.put(URL_REST + 'participant/' + id + '/markpresence', params);
   },

   enrolledParticipant: function(id, params) {
     return $http.post(URL_REST + 'participant/' + id + '/enrolled', params);
   },

   unEnrolledParticipant: function(id, idevent) {
     return $http.delete(URL_REST + 'participant/' + id + '/unenrolled/' + idevent);
   },

   getAvailables: function(id) {
     return $http.get(URL_REST + 'participant/' + id + '/avaiables');
   },

   sendAvailable: function(id, params) {
     return $http.put(URL_REST + 'participant/' + id + '/sendavailable', params);
   },

   getEventsRealized: function(id) {
     return $http.get(URL_REST + 'participant/' + id + '/eventsrealized');
   },

   getRating: function(id) {
     return $http.get(URL_REST + 'participant/' + id + '/rating');
   },

   getHoursMonth: function(id) {
     return $http.get(URL_REST + 'participant/' + id + '/hoursmonth');
   },

   getEventsTrainer: function(id) {
     return $http.get(URL_REST + 'trainer/' + id + '/eventsrealized');
   },

   getRatingTrainer: function(id) {
     return $http.get(URL_REST + 'trainer/' + id + '/rating');
   },

   getHoursMonthTrainer: function(id) {
     return $http.get(URL_REST + 'trainer/' + id + '/hoursmonth');
   },

  }
});
