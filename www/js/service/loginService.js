jmSoftApp.service("LoginService", function($http, jmSoftAppVal){
  return {
    loginData: function(user) {
      console.log(user);
      return $http({
        method: 'POST',
        url: URL_REST_LOGIN + 'auth',
        data: user,
        headers: {'Content-Type': 'application/json'}
      }).then(function(response){
        jmSoftAppVal.token = response.data.Token;
        jmSoftAppVal.user.name = response.data.NamePerson;
        jmSoftAppVal.user.id = response.data.IdUser;

        $http.defaults.headers.common.Authorization = 'Token ' + jmSoftAppVal.token;
        $http.defaults.headers.common.ContentType = 'application/json';

        window.localStorage.setItem("jmSoftAppVal", JSON.stringify(jmSoftAppVal))

        return response;
      });
    },
  }
});
